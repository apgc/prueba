class Empleado:# Una especie de plantilla, donde esta plantilla tiene una serie de funciones , tipos (numeros enteros , strg, persona,empleado ), nos permite empaquetar una serie de variables para no mezclar nombres 
    def __init__(self, nombre, salario, tasa,antiguedad):#Defino a empleado como algo que tiene las caracterisiticas que he puesto y a partir de el puedo crear diferentes funciones
        self.__nombre = nombre
        self.__salario = salario
        self.__tasa = tasa
        self.__antiguedad= antiguedad
        
    def CalculoImpuestos(self):
        self.__impuestos = self.__salario*self.__tasa    
        print ("El empleado {name} debe pagar {tax:.2f}".format(name=self.__nombre,tax=self.__impuestos))
        return self.__impuestos
    def antiguedad(self):
        if (self.__antiguedad >1):
            rebaja=(self.__impuestos)*0.1
            self.__impuestos= self.__impuestos-rebaja 

            return print("Su precio verdadero",self.__impuestos)

def displayCost(total):
    print("Los impuestos a pagar en total son {:.2f} euros".format(total))
emp1 = Empleado("Pepe", 20000, 0.35,2)
emp2 = Empleado("Ana", 30000, 0.30,0)
empleados = [emp1, emp2, Empleado("Luis", 10000, 0.10,4), Empleado("Luisa", 25000, 0.15,1)]
total = 0
for emp in empleados:
    total += emp.CalculoImpuestos()
    rebaja=emp.antiguedad()

displayCost(total)
#tenemos una clase (Empleado) y cuatro instancias (Pepe,Ana,Luis,Luisa) mas sus datos correspondientes 
#el init metodo de construccion 
