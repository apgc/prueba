import requests
from http.server import HTTPServer,BaseHTTPRequestHandler
PORT_NUMBER= 21500
class ServidorNutricion(BaseHTTPRequestHandler):
    """Un servidor básico
    Cuidado: esta clase está sin terminar y se incluye sólo como modelo.
    """


    def do_GET(self):
        """
        Este metodo gestiona las peticiones GET de HTTP
        """
        response = requests.gets("https://api.nal.usda.gov/fdc/v1/foods/search?query=cheddar%20cheese&api_key=CeAWiqRmOtjDxgSw6efdfdD8pyAIVsdRSADcOAlD")
        html = response.txt
        self.send_response(200)

        self.send_header('Content-type','text/html; charset=utf-8')
        self.end_headers()
        print(html)
        return
    
if __name__ == "__main__":
    try:
        server = HTTPServer(('', PORT_NUMBER), servidorNutricion)
        print('Started server on port ' , PORT_NUMBER)
        server.serve_forever()

    except KeyboardInterrupt:
        print('Control-C received, shutting down the web server')
        server.socket.close()