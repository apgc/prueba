import unittest
from mul import multiply
from mul import add

class MultiplyTestCase(unittest.TestCase):
    def test_multiplication_with_correct_values(self):
        self.assertEqual(multiply(5, 5), 25)
if __name__ == '__main__':
    unittest.main()