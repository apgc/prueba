
from http.server import HTTPServer, BaseHTTPRequestHandler

PORT_NUMBER = 21010


class MiServidor(BaseHTTPRequestHandler):

    # Diccionario de compañeros de clase
    companeros = [
        {"nombre": "Ismael", "cuenta": "AI"},
        {"nombre": "Betty", "cuenta": "BT"},
    ]


    def do_GET(self):
        if self.path == "/amigos":
            self.send_response(200)
            self.send_header('Content-type', 'text/html')
            self.end_headers()
            # Construir la tabla HTML
            html_response = "<html><head><title>Amigos</title></head><body><h1>Lista de Amigos</h1><table border='3'><tr><th>Nombre</th><th>Cuenta GitLab</th></tr>"
            for comp in self.companeros:
                html_response += f"<tr><td>{comp['nombre']}</td><td>{comp['cuenta']}</td></tr>"
            html_response += "</table></body></html>"
            self.wfile.write(html_response.encode())
        else:
            self.send_response(404)
            self.send_header('Content-type', 'text/plain')
            self.end_headers()
            self.wfile.write("Página no encontrada".encode())

try:
    server = HTTPServer(('', PORT_NUMBER), MiServidor)
    print('Started httpserver on port', PORT_NUMBER)
    server.serve_forever()

except KeyboardInterrupt:
    print('Control-C received, shutting down the web server')
    server.socket.close()
