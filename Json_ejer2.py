import json


with open('companeros.json', 'r') as f:
    data = json.load(f)

# Obtener nombres y cuentas
nombres = [comp["nombre"] for comp in data["valores"]]
cuentas = [comp["cuenta"] for comp in data["valores"]]

# Imprimir nombres y cuentas
print("Nombres:", nombres)
print("Cuentas:", cuentas)

